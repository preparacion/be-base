/**
 * config/index.js
 *
 * @description :: Loads config values
 * @docs        :: TODO
 */
'use strict'

const dotenv = require('dotenv')
const path = require('path')
const env = process.env.NODE_ENV || 'development'

const envPath = path.resolve(__dirname, `../.envs/${env}`)
dotenv.config({ path: envPath })

const configs = {
  base: {
    env,
    name: process.env.APP_NAME || 'koa-basic-boilerplate',
    host: process.env.APP_HOST || '0.0.0.0',
    port: process.env.APP_PORT || 3100
  },
  production: {},
  development: {},
  test: {
    port: process.env.APP_PORT || 3110,
    host: process.env.APP_HOST || '127.0.0.1'
  }
}

const config = Object.assign(configs.base, configs[env])
module.exports = config
