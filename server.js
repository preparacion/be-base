/**
 * server.js
 *
 * @description :: Loads server configuration
 */
const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const app = new Koa()

const routes = require('./routes')

app.use(bodyParser({
  enableTypes: ['json', 'form'],
  formLimit: '10mb',
  jsonLimit: '10mb'
}))

// routes
app.use(routes.routes(), routes.allowedMethods())

module.exports = app
