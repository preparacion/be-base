/**
 * routes/index.js
 *
 * @description :: Defines routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

// Home routes
const homeApi = require('./api/home')
router.use('', homeApi.routes(), homeApi.allowedMethods())

module.exports = router
