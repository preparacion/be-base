/**
 * router/home.js
 *
 * @description :: Describe the home api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

router.get('/', async (ctx) => {
  ctx.body = {
    success: true,
    message: 'be-base, ok'
  }
})

module.exports = router
